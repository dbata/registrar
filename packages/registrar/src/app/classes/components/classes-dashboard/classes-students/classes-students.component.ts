import {Component, OnInit, ViewChild, Input, EventEmitter, OnDestroy, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {
  AdvancedRowActionComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {AngularDataContext} from '@themost/angular';
import {Observable, Subscription, combineLatest} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {ErrorService, ModalService, ToastService, DIALOG_BUTTONS, LoadingService} from '@universis/common';
// tslint:disable-next-line:max-line-length
import {ActivatedTableService} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import { ClientDataQueryable } from '@themost/client';
import { UserDepartmentsService } from '../../../../registrar-shared/services/userDepartments.service';
import { ActiveDepartmentService } from '../../../../registrar-shared/services/activeDepartmentService.service';
import { AdminService, PermissionMask, Targets} from '@universis/ngx-admin';
import { map } from 'rxjs/operators';
import { TableConfigurationResolver } from '../../../../registrar-shared/table-configuration.resolvers';

@Component({
  selector: 'app-classes-preview-students',
  templateUrl: './classes-students.component.html'
})
export class ClassesStudentsComponent implements OnInit, OnDestroy {

  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  courseClassID: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  private selectedItems = [];
  private userDepartments = [];
  private activeDepartment: any;

  public hasPermissionToRemoveStudents: boolean;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _userDepartments: UserDepartmentsService,
              private _activeDepartment: ActiveDepartmentService,
              private _adminService: AdminService
  ) {
  }

  async ngOnInit() {
    // check if current user has permission to remove students
    const removeStudentsPermission = {
      model: 'StudentCourseClass',
      privilege: 'StudentCourseClass/Remove',
      target: Targets.All,
      mask: PermissionMask.Execute
    };
    // do not await this call
    this._adminService.hasPermission(removeStudentsPermission).then((validationResult: boolean) => {
      this.hasPermissionToRemoveStudents = validationResult;
    }).catch(err => {
      console.error(err);
      // on error, allow the action to be available
      // important note: the api server will always handle privileges correctly
      this.hasPermissionToRemoveStudents = true;
    });
    // get departments outside subscription
    // do not await this call
    this._userDepartments.getDepartments().then((departments) => {
      if (Array.isArray(departments)) {
        this.userDepartments = departments.map((department: {id: number | string}) => department.id);
      }
    }).catch(err => {
      console.error(err);
      this._errorService.showError(err);
    });
    try {
      // get active department outside subscription
      this.activeDepartment = await this._activeDepartment.getActiveDepartment();
      if (this.activeDepartment == null) {
        throw new Error('The active department cannot be determined.');
      }
      this.subscription = combineLatest(
         this._activatedRoute.params,
         this._activatedRoute.data
         ).pipe(
          map(
            ([params, data]) => ({params, data})
          )
         ).subscribe(async (results) => {
        try {
          const params = results.params;
          const data = results.data;
          this.courseClassID = params.id;
          this._activatedTable.activeTable = this.students;
          // prepare filter for students that do not belong to the user departments
          const otherDepartmentsQuery = this._context.model('StudentCourseClasses')
            .asQueryable()
            .filter('studentDepartment ne departments()');
          const hasOtherDepartmentStudents =
              await otherDepartmentsQuery
                // filter by current course class
                .and('courseClass').equal(this.courseClassID)
                // but require the course to be offered by the active department
                .and('courseClass/course/department').equal(this.activeDepartment.id.toString())
                .select('id')
                // check if at least one item exists
                .getItem();
          const config = AdvancedTableConfiguration.cast(data.tableConfiguration);
          // set config model accordingly
          if (hasOtherDepartmentStudents) {
            config.model = `CourseClasses/${encodeURIComponent(this.courseClassID)}/sharedStudents`;
          } else {
            config.model = `CourseClasses/${encodeURIComponent(this.courseClassID)}/students`;
          }
          // set table config
          this.students.config = config;
          // and init table
          this.students.fetch();

          if (data.searchConfiguration) {
            this.search.form = data.searchConfiguration;
            Object.assign(this.search.form, { courseClass: this.courseClassID });
            this.search.ngOnInit();
          }
        } catch (err) {
          console.error(err);
          this._errorService.showError(err, {
            continueLink: '.'
          });
        }
      });

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        try {
          if (fragment && fragment === 'reload') {
            this.students.fetch(true);
          }
        } catch (err) {
          console.error(err);
          this._errorService.showError(err, {
            continueLink: '.'
          });
        }
      });

    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  remove() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseClassID = params.id;
      if (this.students && this.students.selected && this.students.selected.length) {
        const items = this.students.selected.map(item => {
          return {
            courseClass: this.courseClassID,
            student: item.studentId
          };
        });
        return this._modalService.showWarningDialog(
          this._translateService.instant('Classes.RemoveStudentTitle'),
          this._translateService.instant('Classes.RemoveStudentMessage'),
          DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('StudentCourseClasses').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Classes.RemoveStudentsMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Classes.RemoveStudentsMessage.one' : 'Classes.RemoveStudentsMessage.many')
                  , {value: items.length})
              );
              this.students.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });

      }
    });
  }

  async addToSectionManually() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // filter items by user departments
      this.selectedItems = items.filter((item) => this.userDepartments.includes(item.studentDepartment));
      // validate sections and show message
      const sections = await this._context.model('CourseClassSections')
        .asQueryable()
        .where('courseClass').equal(this.courseClassID)
        .select('count(id) as total')
        .getItem();
      if (!(sections && sections.total)) {
        return this._modalService.showInfoDialog(this._translateService.instant('Classes.AddToSectionManually.Title'),
          this._translateService.instant('Classes.AddToSectionManually.NoSections'), DIALOG_BUTTONS.Ok);
      }
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          data: {
            'courseClass': this.courseClassID
          },
          items: this.selectedItems,
          formTemplate: 'Students/addToSection',
          modalTitle: 'Classes.AddToSectionManually.Title',
          description: 'Classes.AddToSectionManually.Description',
          errorMessage: 'Classes.AddToSectionManually.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeAddToSectionManually()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }

  async addToSectionAutomated() {
    // pass
  }

  async removeFromSection() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter(item => item.section && this.userDepartments.includes(item.studentDepartment));
      this._loadingService.hideLoading();
      // open modal
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Classes.RemoveFromSection.Title',
          description: 'Classes.RemoveFromSection.Description',
          errorMessage: 'Classes.RemoveFromSection.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeRemoveFromSection()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }

  executeRemoveFromSection() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const removeFromSection = {
              id: item.id,
              student: item.studentId,
              courseClass: this.courseClassID,
              course: item.courseId,
              section: null,
              $state: 2
            };
            await this._context.model('StudentCourseClasses').save(removeFromSection);
            result.success += 1;
            try {
              await this.students.fetchOne({
                  id: item.id
              });
            } catch (err) {
              console.log(err);
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  executeAddToSectionManually() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.courseClassSection == null) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      const section = data.courseClassSection;
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // assign data
            data.id = item.id;
            data.section = parseInt(section, 10);
            data.course = item.courseId;
            data.student = item.studentId;
            delete data.courseClassSection;
            // and save
            await this._context.model('StudentCourseClasses').save(data);
            result.success += 1;
            try {
              await this.students.fetchOne({
                  id: item.id
              });
            } catch (err) {
              console.log(err);
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async editStudentClassStatus() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter(item => item.studentStatus === 'active' && this.userDepartments.includes(item.studentDepartment));
      this._loadingService.hideLoading();
      // open modal
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: 'StudentCourseClasses/editStudentClassStatus',
          modalTitle: 'Classes.EditStudentClassStatus.Title',
          description: 'Classes.EditStudentClassStatus.Description',
          errorMessage: 'Classes.EditStudentClassStatus.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeEditStudentClassStatus()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeEditStudentClassStatus() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (!data.status || !data.status.hasOwnProperty('id')) {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });

            // save only studentCourseClasses whose studentClassStatus is going to be altered
            if (item.status !== data.status.name) {
              data.id = item.id;
              await this._context.model('StudentCourseClasses').save(data);
              result.success += 1;
              try {
                await this.students.fetchOne({
                  id: item.id
                });
              } catch (err) {
                console.log(err);
              }
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.students && this.students.lastQuery) {
      const lastQuery: ClientDataQueryable = this.students.lastQuery;
      if (lastQuery != null) {
        if (this.students.smartSelect) {
          // get items
          const selectArguments = ['student/id as studentId',
            'id', 'courseClass/course/id as courseId', 'section',
            'student/studentStatus/alternateName as studentStatus',
            'status/name as status',
            'absences', 'examGrade',
            'student/department/id as studentDepartment'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.students.unselected && this.students.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.students.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.students.selected.map( (item) => {
            return {
              studentId: item.studentId,
              id: item.id,
              courseId: item.courseId,
              section: item.section,
              studentStatus: item.studentStatus,
              status: item.status,
              absences: item.absences,
              examGrade: item.examGrade,
              studentDepartment: item.studentDepartment
            };
          });
        }
      }
    }
    return items;
  }

  async removeStudentsFromClass() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter(item =>
        // consider active students
        (item.studentStatus === 'active') &&
        // without grade from any exam
        (item.examGrade == null) &&
        // without absences
        (item.absences == null || item.absences === 0) &&
        // and of the user's departments
        (this.userDepartments.includes(item.studentDepartment)));
      this._loadingService.hideLoading();
      // open modal
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Classes.RemoveStudentsFromClass.Title',
          description: 'Classes.RemoveStudentsFromClass.Description',
          errorMessage: 'Classes.RemoveStudentsFromClass.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeRemoveStudentsFromClass()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeRemoveStudentsFromClass() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // try to remove student from class
            await this._context.model(`StudentCourseClasses/${item.id}/removeStudentFromClass`).save(null);
            result.success += 1;
          } catch (err) {
            // log error
            console.error(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        // refetch data
        this.students.fetch(true);
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

}
