import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
// tslint:disable-next-line:max-line-length
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '@universis/ngx-tables';
import {AngularDataContext} from '@themost/angular';
import {DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {Subscription, combineLatest} from 'rxjs';
import {ActivatedTableService} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import { TableConfigurationResolver } from '../../../../registrar-shared/table-configuration.resolvers';
import { map } from 'rxjs/operators';
// tslint:disable-next-line:max-line-length





@Component({
  selector: 'app-classes-sections',
  templateUrl: './classes-sections.component.html',
  styleUrls: []
})
export class ClassesSectionsComponent implements OnInit, OnDestroy {

  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('sections') sections: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  courseClassID: any ;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  private subscription: Subscription;


  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext,
              private _resolver: TableConfigurationResolver,
              private _loadingService: LoadingService

  ) { }


  async ngOnInit() {
    this.subscription = combineLatest(this._activatedRoute.params, this._resolver.get('CourseClassSections'))
    .pipe(
      map(([params,tableConfiguration]) => ({params,tableConfiguration}))
    ).subscribe(async ({params,tableConfiguration}) => {
      this.courseClassID = params.id;
      this._activatedTable.activeTable = this.sections;

      this.sections.query = this._context.model('CourseClassSections')
        .where('courseClass').equal(this.courseClassID)
        .expand('instructor1')
        .prepare();
      this.sections.config = AdvancedTableConfiguration.cast(tableConfiguration);
      this.sections.fetch();
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.sections.fetch(true);
      }
    });
  }
  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }
  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  remove() {
    if (this.sections && this.sections.selected && this.sections.selected.length) {
      const items = this.sections.selected.map( item => {
        return {
          courseClass: this.courseClassID,
          id: item.id,
          instructor1: null,
          instructor2: null,
          instructor3: null
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Classes.RemoveSectionTitle'),
        this._translateService.instant('Classes.RemoveSectionMessage'),
        DIALOG_BUTTONS.OkCancel).then( result => {
          if (result === 'ok') {
            this._loadingService.showLoading();
            return this._context.model('courseClassSections').save(items).then(() => {
              return this._context.model('courseClassSections').remove(items).then(() => {
                this._toastService.show(
                  this._translateService.instant('Classes.RemoveSectionsMessage.title'),
                  this._translateService.instant((items.length === 1 ?
                    'Classes.RemoveSectionsMessage.one' : 'Classes.RemoveSectionsMessage.many')
                    , { value: items.length })
                );
                this.sections.fetch(true);
                this._loadingService.hideLoading();
              });
            }).catch(err => {
              this._loadingService.hideLoading();
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
      });

    }
  }
}
