import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsGraduatedComponent } from './students-graduated.component';

describe('StudentsGraduatedComponent', () => {
  let component: StudentsGraduatedComponent;
  let fixture: ComponentFixture<StudentsGraduatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsGraduatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsGraduatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
