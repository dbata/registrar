import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-departments-dashboard-overview-study-programs',
  templateUrl: './departments-dashboard-overview-study-programs.component.html',
})
export class DepartmentsDashboardOverviewStudyProgramsComponent  implements OnInit {
  public departmentsId: any;
  public model: any;
  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {}

  async ngOnInit() {
    this.departmentsId = this._activatedRoute.snapshot.params.id;

    this.model = await this._context.model('StudyPrograms')
      .where('department').equal(this._activatedRoute.snapshot.params.id)
      .expand('studyLevel')
      .getItems();
  }
}
