import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsPreviewGeneralComponent } from './study-programs-preview-general.component';

describe('StudyProgramsPreviewGeneralComponent', () => {
  let component: StudyProgramsPreviewGeneralComponent;
  let fixture: ComponentFixture<StudyProgramsPreviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsPreviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsPreviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
