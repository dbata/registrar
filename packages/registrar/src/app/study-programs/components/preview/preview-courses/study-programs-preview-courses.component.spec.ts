import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsPreviewCoursesComponent } from './study-programs-preview-courses.component';

describe('StudyProgramsPreviewCoursesComponent', () => {
  let component: StudyProgramsPreviewCoursesComponent;
  let fixture: ComponentFixture<StudyProgramsPreviewCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsPreviewCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsPreviewCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
