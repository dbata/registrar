import {ModuleWithProviders, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {AppEventService, SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {
  StudyProgramsPreviewFormComponent
} from './components/preview/preview-general/study-programs-preview-form.component';
import { ProgramCoursePreviewFormComponent } from './components/program-course-preview/program-course-preview-general/program-course-preview-form.component';
import { EditCoursesComponent } from './components/preview/edit-courses/edit-courses.component';
import {MostModule} from '@themost/angular';
import {RouterModalModule} from '@universis/common/routing';
import {AdvancedFormsModule} from '@universis/forms';
import {SettingsSharedModule} from '../settings-shared/settings-shared.module';
import {CoursesSharedModule} from '../courses/courses.shared';
import {AddCoursesComponent} from './components/preview/add-courses/add-courses.component';
import {SpecializationCourseProgramCourseResolver} from './services/program-course.resolver';
import {AddGroupCoursesComponent} from './components/preview/group-courses/add-group-courses/add-group-courses.component';
// tslint:disable-next-line:max-line-length
import {GroupCoursesDetailsPercentComponent} from './components/preview/group-courses/group-courses-details/group-courses-details-percent/group-courses-details-percent.component';
import {
  SemesterRulesConfigurationResolver,
  SemesterRulesSearchResolver
} from './components/preview/preview-semester-rules/semester-rules-config.resolver';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule,
    FormsModule,
    MostModule,
    RouterModalModule,
    AdvancedFormsModule,
    SettingsSharedModule,
      CoursesSharedModule
  ],
  declarations: [
    StudyProgramsPreviewFormComponent,
    ProgramCoursePreviewFormComponent,
    EditCoursesComponent,
    AddCoursesComponent,
    AddGroupCoursesComponent,
    GroupCoursesDetailsPercentComponent

  ],
  entryComponents: [
    EditCoursesComponent,
    AddCoursesComponent,
    AddGroupCoursesComponent,
    GroupCoursesDetailsPercentComponent
  ],
  exports: [
    StudyProgramsPreviewFormComponent,
    ProgramCoursePreviewFormComponent,
    EditCoursesComponent
  ],
  providers: [
  ]
})
export class StudyProgramsSharedModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: StudyProgramsSharedModule,
      providers: [
        SpecializationCourseProgramCourseResolver,
        SemesterRulesConfigurationResolver,
        SemesterRulesSearchResolver
      ]
    };
  }

  constructor(private _translateService: TranslateService, private appEvent: AppEventService) {
    const sources = environment.languages.map((language: string) => {
      return import(`./i18n/study-programs.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
        return Promise.resolve();
      })
    });
    Promise.all(sources).then(() => {
      // emit event
      this.appEvent.add.next({
        service: this._translateService,
        type: this._translateService.setTranslation
      });
    }).catch((err) => {
      console.error('An error occurred while loading shared module');
      console.error(err);
    });
  }

}
