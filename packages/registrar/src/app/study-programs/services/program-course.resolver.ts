import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Args} from '@themost/client';

@Injectable()
export class SpecializationCourseProgramCourseResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext) { }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any> | any {
    if (route.params.id) {
      return this._context.model(`SpecializationCourses/${route.params.id}/studyProgramCourse`)
          .asQueryable(route.data.serviceQueryParams || {
            $expand: 'course,studyProgram'
          })
          .getItem();
    }
    return null;
  }
}
