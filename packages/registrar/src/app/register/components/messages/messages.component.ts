import { Component, OnInit, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { AdvancedTableComponent, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { Subscription, Observable } from 'rxjs';
import * as StudyProgramRegisterActionMessageTable from './StudyProgramRegisterActionMessageTable.json';
import * as StudyProgramRegisterActionMessageSearch from './StudyProgramRegisterActionMessageSearch.json';
import { AngularDataContext } from '@themost/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { UserActivityService, AppEventService, LoadingService, ModalService, ErrorService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedTableService } from '@universis/ngx-tables';
import { ClientDataQueryable } from '@themost/client';
import { RequestActionComponent } from '../../../requests/components/request-action/request-action.component';

@Component({
  selector: 'app-register-message-list',
  templateUrl: './messages.component.html'
})
export class MessagesComponent implements OnInit, OnDestroy {

  public recordsTotal: any;
  private dataSubscription: Subscription;
  @Input() tableConfiguration: any = StudyProgramRegisterActionMessageTable;
  @Input() searchConfiguration: any = StudyProgramRegisterActionMessageSearch;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  selectedItems: any[];

  constructor(private _context: AngularDataContext,
    private _router: Router,
    private _activatedTable: ActivatedTableService,
    private _activatedRoute: ActivatedRoute,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _appEvent: AppEventService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {

      this._activatedTable.activeTable = this.table;
      // set search form
      this.search.form = this.searchConfiguration;
      this.search.ngOnInit();
      // set table config and recall data
      if (this.tableConfiguration) {
        this.table.config = this.tableConfiguration;
        this.advancedSearch.getQuery().then(res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }

      this._userActivityService.setItem({
        category: this._translateService.instant('Register.Messages'),
        description: this._translateService.instant('List'),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

}
