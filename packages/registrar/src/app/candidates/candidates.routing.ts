import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CandidatesTableComponent} from './components/candidates/candidates-table.component';
import {ActiveDepartmentResolver, CurrentAcademicYearResolver} from '../registrar-shared/services/activeDepartmentService.service';
import {CandidatesHomeComponent} from './components/candidates-home/candidates-home.component';
import {AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormResolver} from '@universis/forms';
import { ExportFromCandidateSourceComponent } from './components/candidates-import/export-from-candidate-source/export-from-candidate-source.component';
import { CandidateUploadActionsComponent } from './components/candidate-upload-actions/candidate-upload-actions.component';
import { CandidateUploadActionsHomeComponent } from './components/candidate-upload-actions/candidate-upload-actions-home/candidate-upload-actions-home.component';
import { CandidateUploadActionResultsComponent } from './components/candidate-upload-actions/candidate-upload-action-results/candidate-upload-action-results.component';
import { SearchConfigurationResolver, TableConfigurationResolver } from '../registrar-shared/table-configuration.resolvers';

const routes: Routes = [
  {
    path: '',
    component: CandidatesHomeComponent,
    data: {
      title: 'Candidate students'
    },
    children: [
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/current'
      },
      {
        path: 'list/:list',
        component: CandidatesTableComponent,
        data: {
          title: 'Candidate students list'
        },
        resolve: {
          currentYear: CurrentAcademicYearResolver,
          tableConfiguration: TableConfigurationResolver,
          searchConfiguration: SearchConfigurationResolver,
          department: ActiveDepartmentResolver
        },
        children: [
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'person($expand=gender,nationality),studyProgram($expand=department),inscriptionMode'
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          },
          {
            path: 'export',
            pathMatch: 'full',
            component: ExportFromCandidateSourceComponent,
            outlet: 'modal'
          }
        ]
      }
    ]
  },
  {
    path: 'upload-actions',
    component: CandidateUploadActionsHomeComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: CandidateUploadActionsComponent,
        data: {
          model: 'CandidateStudentUploadActions',
          list: 'list'
        },
        resolve: {
          tableConfiguration: TableConfigurationResolver,
          searchConfiguration: SearchConfigurationResolver
        },
        children: [
          {
            path: 'item/:action/results',
            component: CandidateUploadActionResultsComponent,
            outlet: 'modal'
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class CandidatesRoutingModule {
}
