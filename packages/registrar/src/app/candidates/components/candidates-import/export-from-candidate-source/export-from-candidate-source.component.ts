import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { AngularDataContext } from "@themost/angular";
import { LoadingService } from "@universis/common";
import { RouterModalOkCancel } from "@universis/common/routing";
import { AdvancedFormComponent } from "@universis/forms";
import { Subscription } from "rxjs";

@Component({
  selector: "app-export-from-candidate-source",
  templateUrl: "./export-from-candidate-source.component.html",
  styleUrls: ["./export-from-candidate-source.component.scss"],
})
export class ExportFromCandidateSourceComponent
  extends RouterModalOkCancel
  implements OnInit, OnDestroy
{
  public lastError: any;
  private formChangeSubscription: Subscription;

  @ViewChild("formComponent") formComponent: AdvancedFormComponent;

  constructor(
    protected _router: Router,
    protected _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _translateService: TranslateService,
    private _loadingService: LoadingService
  ) {
    // call super constructor
    super(_router, _activatedRoute);
    this.modalTitle = this._translateService.instant("Candidates.ExportAction.Title");
    this.okButtonText = this._translateService.instant("Candidates.ImportAction.Export");
    this.modalClass = "modal-lg";
  }

  ngOnInit() {
    this.formChangeSubscription = this.formComponent.form.change.subscribe(
      (event) => {
        if (Object.prototype.hasOwnProperty.call(event, "isValid")) {
          // enable or disable button based on form status
          this.okButtonDisabled = !event.isValid;
        }
        // validate distinctly because of multiple formio change emits (first load)
        const candidateSource =
          event.data &&
          event.data.candidateSource &&
          event.data.candidateSource.id;
        if (!candidateSource) {
          this.okButtonDisabled = true;
        }
      }
    );
  }

  async ok() {
    try {
      this._loadingService.showLoading();
      // clear last error
      this.lastError = null;
      // get candidate source
      const candidateSource =
        this.formComponent &&
        this.formComponent.form &&
        this.formComponent.form.formio &&
        this.formComponent.form.formio.data &&
        this.formComponent.form.formio.data.candidateSource;
      if (!candidateSource || Object.keys(candidateSource).length === 0) {
        return this.close();
      }
      // fetch the xlsx file
      await new Promise((resolve, reject) => {
        const headers = new Headers();
        // get service headers
        const serviceHeaders = this._context.getService().getHeaders();
        Object.keys(serviceHeaders).forEach((key) => {
          if (serviceHeaders.hasOwnProperty(key)) {
            headers.set(key, serviceHeaders[key]);
          }
        });
        // set accept header
        headers.set(
          "Accept",
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        );
        const fileURL = this._context
          .getService()
          .resolve(`/CandidateSources/${candidateSource.id}/exportCandidates`);
        // fetch exam grading sheet
        fetch(fileURL, {
          headers: headers,
          credentials: "include",
        })
          .then((response) => {
            return response.blob();
          })
          .then((blob) => {
            const objectUrl = window.URL.createObjectURL(blob);
            const a = document.createElement("a");
            document.body.appendChild(a);
            a.setAttribute("style", "display: none");
            a.href = objectUrl;
            const name = candidateSource.name.replace(/[/\\?%*:|"<>]/g, "-");
            const extension = "xlsx";
            const downloadName = `${name}.${extension}`;
            a.download = downloadName;
            a.click();
            window.URL.revokeObjectURL(objectUrl);
            a.remove();
            return resolve(null);
          })
          .catch((err) => {
            return reject(err);
          });
      });
      // and close
      return this.close();
    } catch (err) {
      this.lastError = err;
    } finally {
      this._loadingService.hideLoading();
    }
  }

  cancel() {
    return this.close();
  }

  ngOnDestroy(): void {
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }
}
