import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import * as hljs from 'highlight.js/lib/highlight';
import * as prettier from 'prettier/standalone';
import * as htmlParser from 'prettier/parser-html';
import * as angularParser from 'prettier/parser-angular';
import * as typescriptParser from 'prettier/parser-typescript';
import * as typescriptLanguage from 'highlight.js/lib/languages/typescript';
import {HttpClient} from '@angular/common/http';
import * as $ from 'jquery';
import * as xmlLanguage from 'highlight.js/lib/languages/xml';
import * as javascriptLanguage from 'highlight.js/lib/languages/javascript';

hljs.registerLanguage('xml', xmlLanguage);
hljs.registerLanguage('javascript', javascriptLanguage);
hljs.registerLanguage('typescript', typescriptLanguage);


function escapeHTML(unsafe) {
  if (typeof unsafe == null) {
    return;
  }
  return unsafe
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#039;');
}

function unEscapeHTML(unsafe) {
  if (typeof unsafe == null) {
    return;
  }
  return unsafe
    .replace(/&amp;/g, '&')
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>')
    .replace(/&quot;/g, '"')
    .replace(/&#039;/g, '\'');
}

function escapeTypescript(unsafe) {
  if (typeof unsafe == null) {
    return;
  }
  return unsafe.replace(/"/g, '&quot;')
    .replace(/'/g, '&#039;')
    .replace(/@/g , '&#064;')
    .replace(/\{/g, '&#123;')
    .replace(/\}/g, '&#125;')
    .replace(/\(/g, '&#40;')
    .replace(/\)/g, '&#41;')
    .replace(/\//g, '&#47;')
    .replace(/=/g, '&#61;')
    .replace(/_/g, '&#95;');
}

function unescapeTypescript(unsafe) {
  if (typeof unsafe == null) {
    return;
  }
  return unsafe.replace(/&quot;/g, '"')
    .replace(/&#039;/g, '\'')
    .replace(/&#064;/g, '@')
    .replace(/&#123;/g, '{')
    .replace(/&#125;/g, '}')
    .replace(/&#40;/g, '(')
    .replace(/&#41;/g, ')')
    .replace(/&#47/g, '/')
    .replace(/&#61/g, '=')
    .replace(/&#95;/g, '_');
}

@Component({
  selector: 'app-highlight-include',
  template: `
      <div class="card">
          <div class="card-body">
              <button class="btn btn-copy btn-sm btn-secondary" (click)="copyCode()">Copy code</button>
              <pre><code #codeElement class="hljs"></code></pre>
          </div>
      </div>
  `,
  styles: [`
      .card-body {
          padding: 0;
      }

      pre {
          margin: 0;
      }

      code.hljs {
          padding: 1.5rem;
          font-size: 12px;
      }

      .btn-copy {
          position: absolute;
          top: 4px;
          right: 4px;
          opacity: 0.5;
      }
  `]
})
export class HighlightIncludeComponent implements OnInit {

  @Input() highlight: string;
  @ViewChild('codeElement') codeElement: ElementRef;

  public innerHTML: string;
  private innerSource: string;

  constructor(private element: ElementRef, private http: HttpClient) {
  }

  copyCode() {
    const selectBox = document.createElement('textarea');
    selectBox.style.position = 'fixed';
    selectBox.style.left = '0';
    selectBox.style.top = '0';
    selectBox.style.opacity = '0';
    selectBox.value = unEscapeHTML(this.innerSource);
    document.body.appendChild(selectBox);
    selectBox.focus();
    selectBox.select();
    document.execCommand('copy');
    document.body.removeChild(selectBox);
  }

  ngOnInit() {

    if (this.highlight) {
      try {
        //  Just use parse to check if input is a JSON string. 
        //  For different input just continue checking on catch block
        JSON.parse(this.highlight);
        this.innerSource = this.highlight;
        this.codeElement.nativeElement.innerHTML = this.highlight;
        hljs.highlightBlock(this.codeElement.nativeElement);
      } catch (err) {

        const hashIndex = this.highlight.indexOf('#');
        let source = this.highlight;
        let sourceElement;
        if (hashIndex > 0) {
          source = this.highlight.substr(0, hashIndex);
          sourceElement = this.highlight.substr(hashIndex);
        }

        if (/.ts$/.test(source)) {
          return this.http.get(source, {responseType: 'text'}).subscribe((ts: string) => {
            this.innerSource = prettier.format(ts, {
              parser: 'typescript',
              printWidth: 140,
              useTabs: false,
              plugins: { parser: typescriptParser }
            });
            this.codeElement.nativeElement.innerHTML = escapeTypescript(this.innerSource);
            hljs.highlightBlock(this.codeElement.nativeElement, typescriptLanguage);
          });
        }

        this.http.get(source, {responseType: 'text'}).toPromise().then((html: string) => {
          if (html) {
            const targetElement = sourceElement ? $(html).find(sourceElement) : $(html);
            if (targetElement) {
              // format html
              this.innerSource = prettier.format(targetElement.html(), {
                parser: 'html',
                printWidth: 140,
                useTabs: false,
                tabWidth: 2,
                htmlWhitespaceSensitivity: 'ignore',
                jsxBracketSameLine: true,
                plugins: { parsers: htmlParser }
              });
              const innerHTML = escapeHTML(this.innerSource);
              if (this.codeElement) {
                this.codeElement.nativeElement.innerHTML = innerHTML;
                hljs.highlightBlock(this.codeElement.nativeElement);
              }
            }
          }
        });
        
      }
    }
  }

}
